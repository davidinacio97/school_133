<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 21.12.2014
 * Time: 15:36
 */

/**
 * Class errorManager Error-Manager Klasse interpretiert die Fehler und gibt diese Benutzerfreundlich-zurück.
 */
class errorManager
{
    /**
     * Gibt die Fehlermeldung zurück.
     * @param $errorId Error-Id
     */
    function errorMessage($errorId)
    {
        switch($errorId)
        {
            case "usernameExists":
                return $this->echoErrorDiv("Benutzername exisitert bereits. Bitte anderen Benutzernamen wählen");
            case "surveyAlreadyFilled":
                return $this->echoErrorDiv("Sie haben die Umfrage bereits ausgefüllt. Sie wurden direkt zur Auswertung weitergeleitet.");
            case "logginIncorrect":
                return $this->echoErrorDiv("Login nicht korrekt.");
            case "fillSurveyFirst":
                return $this->echoErrorDiv("Sie müssen zunächst die Umfrage ausfüllen um die Auswertung einzusehen.");

        }
    }

    /**
     * Gibt die Fehlermeldung in Formatierter-HTML Form zurück.
     * @param $errorMessage Fehlermeldung.
     */
    function echoErrorDiv($errorMessage)
    {
        echo "<div class='row'><p class='help-block text-danger text-center'>".$errorMessage."</p></div>";
    }

}