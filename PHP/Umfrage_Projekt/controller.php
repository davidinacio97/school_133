<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 21.12.2014
 * Time: 14:31
 */

/**
 * Binde Datenbank Klasse ein.
 */
require('database.php');

/**
 * Binde Auswertung-Heper ein.
 */
require('auswertungHelper.php');

/**
 * Class controller Controller-Klasse verarbeitet Anfragen.
 */
class controller
{
    /**
     * @var database Datenbank-Objekt
     */
    private  $db;

    /**
     * Inititalisiert das Datenbank-Objekt.
     */
    function  __construct()
    {
        $this->db = new database();
    }

    /**
     * Loggt den User ein.
     * @param $username Benutzername
     * @param $password Passwort
     */
    function loginUser($username, $password)
    {
        // Frage Datenbank ab, ob diese Kombination von Benutzername und Passwort vorhanden ist.
        if ($this->db->existsUser($username, $password))
        {
            // Sessions setzen und auf Umfrage weiterverweisen.
            $_SESSION['loggedin'] = true;
            $_SESSION['userId'] = $this->db->getUserId($username);
            header('Location: index.php?ToShow=umfrage');
        }
        else
        {
            $_SESSION['loggedin'] = false;
            header('Location: index.php?ToShow=login&error=logginIncorrect#content');
        }
    }

    /**
     * Registriert einen Benutzer
     * @param $username Benutzername
     * @param $firstname Vorname
     * @param $lastname Nachname
     * @param $email E-Mail
     * @param $password Passwort
     */
    function registUser($username, $firstname, $lastname, $email, $password)
    {
        // Überprüfe ob Benutzername bereits vorhanden ist, falls ja --> zurück auf Registrieren Formular mit Fehlermeldung
        if($this->db->existsUsername($username))
        {
            header('Location: index.php?ToShow=register&error=usernameExists&username='.$username.'&firstname='.$firstname.'&lastname='.$lastname.'&email='.$email.'#content');
            return;
        }

        // Benutzer registrieren.
        if($this->db->registUser($username,$firstname, $lastname,  $email, $password))
        {
            $this->loginUser($username, $password);
        }
        else
        {
            header('Location: index.php?ToShow=register');
        }

    }

    /**
     * Speichert die Antworten der Umfrage in die Datenbank.
     * @param $postData Antworten.
     */
    function enterSurveyAnswers($postData)
    {
        foreach($_SESSION['questions'] as $x)
        {
            $this->db->enterAnswer(sha1($_SESSION['userId']), $x['id'], $postData[$x['name']]);
        }

        header('Location: index.php?ToShow=auswertung');
    }

    /**
     * Gibt die Fragen für die Umfrage zurück.
     * @return array|bool Falls Abfrage erfolgreich Fragen, andernfalls False zurückgeben
     */
    function getQuestions()
    {
        return $this->db->getQustions();
    }

    /**
     * Gibt die Anzahl Antworten auf eine Frage zurück.
     * @param $questionId Frage-Id
     * @return bool|string Anzahl falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    function countSameQuestionAnswered($questionId)
    {
        return $this->db->countSameQuestionAnswered($questionId);
    }

    /**
     * @param $questionId
     * @return bool
     */
    function getAuswertungText($questionId)
    {
        return $this->db->getQuestionText($questionId);
    }

    /**
     * Gibt zurück ob der User die Umfrage ausgefüllt hat.
     * @param $userid Benutzer-Id
     * @return bool Boolean, ob der Benutzer die Umfrage ausgefüllt hat.
     */
    public function userFilledSurvey($userid)
    {
        return $this->db->userFilledSurvey($userid);
    }

    /**
     * Gibt die Resultattabelle zu einer Frage-Id zurück.
     * @param $questionId  Frage-Id
     */
    function getResult($questionId)
    {
        // Array mit Auswertung-Helper Objekten
        $auswertungData = array();

        // Holle alle möglichen Antworten zu der Frage.
        $answers = $this->db->getAnswers($questionId);

        // Für jede Antwort, hole die Anzahl abstimmungen zu dieser Frage
        foreach($answers as $ans)
        {
            $helper = new auswertungHelper();
            $helper->answerId = $ans['id'];
            $helper->questionId = $questionId;

            if($this->countSameQuestionAnswered($questionId) == 0)
            {
                $helper->percantage = 0;
            }
            else
            {
                $helper->percantage = $this->db->countQuestionAnswers($questionId, $ans['id']) / $this->countSameQuestionAnswered($questionId);
            }

            $auswertungData[] = $helper;

        }

        // Für jedes Auswertung-Helper-Objekt füge eine Tabellenzeile ein.
        foreach ($auswertungData as $data)
        {
            echo "<tr style='padding-bottom: 10px;' width='100%'>";
            echo  "<td width='20%'>" . $this->db->getAnswerText($data->answerId) . "</td>";
            echo "<td width='15%'>" . $this->roundAuswertungPercantage($data->percantage) . "%". "</td>";
            echo "<td>
                    <div style='position: relative; width: 100%;'>
                        <div style='color:#18BC9C; position: absolute; background-color: #18bc9c; width: ".($data->percantage * 100). "%"."'>". $this->roundAuswertungPercantage($data->percantage) . "%"." </div>

                    </div>
                </td>";
            echo "</tr>";
        }
    }

    /**
     * Rundet den Anteil auf 2 Stellen nach dem Komma.
     * @param $percantage Anteil
     * @return float|int gerundetes Resultat.
     */
    function roundAuswertungPercantage($percantage)
    {
        if($percantage == 0)
        {
            return 0;
        }
        else
        {
            return round($percantage * 100, 2);
        }
    }
}

