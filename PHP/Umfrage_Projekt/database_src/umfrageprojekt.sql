-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 21. Dez 2014 um 22:44
-- Server Version: 5.6.21
-- PHP-Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `umfrageprojekt`
--
CREATE DATABASE IF NOT EXISTS `umfrageprojekt` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `umfrageprojekt`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `antworten`
--

CREATE TABLE IF NOT EXISTS `antworten` (
`id` int(11) NOT NULL,
  `antwort` varchar(100) NOT NULL,
  `fk_frage` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `antworten`
--

INSERT INTO `antworten` (`id`, `antwort`, `fk_frage`) VALUES
(1, 'Leonardo DiCaprio', 1),
(2, 'Johnny Depp', 1),
(3, 'Jennifer Lawrence', 2),
(5, 'Brad Pitt', 1),
(6, 'George Clooney', 1),
(7, 'Jessica Alba', 2),
(8, 'Natalie Portman', 2),
(9, '12 years a slave', 3),
(10, 'Argo', 3),
(11, 'Avatar - Aufbruch nach Pandora', 3),
(12, 'The Wolf of Wall Street', 3),
(13, 'Skyfall (Adele)', 4),
(14, 'Let it go (Robert Lopez)', 4),
(15, 'Man or Muppet (Bret McKenzie)', 4),
(16, 'Alfonso Cuaron', 5),
(17, 'Steven Spielberg', 5),
(18, 'Ang Lee', 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fragen`
--

CREATE TABLE IF NOT EXISTS `fragen` (
`id` int(11) NOT NULL,
  `frage` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `fragen`
--

INSERT INTO `fragen` (`id`, `frage`, `name`) VALUES
(1, 'Wählen Sie den besten Hauptdarsteller', 'bestActor'),
(2, 'Wählen Sie die beste Hauptdarstellerin', 'bestActress'),
(3, 'Wählen Sie den besten Film', 'bestMovie'),
(4, 'Wählen Sie den besten Soundtrack', 'bestSoundtrack'),
(5, 'Wählen Sie den besten Regisseur', 'bestRegie');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(6) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `username`, `email`, `password`) VALUES
(2, 'test11', 'test11', 'test11', 'test11@test.com', '7e27f2ae15d1b64393f4db20cfdf11e3'),
(6, 'test12', 'test12', 'test12', 'test12@test.com', 'ac1c2ef47227b7c31843d070b56bdc85');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_antwort`
--

CREATE TABLE IF NOT EXISTS `user_antwort` (
`id` int(11) NOT NULL,
  `fk_userId` int(11) NOT NULL,
  `fk_frageId` int(11) NOT NULL,
  `fk_antwortId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user_antwort`
--

INSERT INTO `user_antwort` (`id`, `fk_userId`, `fk_frageId`, `fk_antwortId`) VALUES
(26, 2, 1, 1),
(27, 2, 2, 7),
(28, 2, 3, 12),
(29, 2, 4, 13),
(30, 2, 5, 16),
(31, 6, 1, 2),
(32, 6, 2, 8),
(33, 6, 3, 9),
(34, 6, 4, 14),
(35, 6, 5, 17);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `antworten`
--
ALTER TABLE `antworten`
 ADD UNIQUE KEY `id` (`id`,`antwort`);

--
-- Indizes für die Tabelle `fragen`
--
ALTER TABLE `fragen`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`,`frage`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`,`username`);

--
-- Indizes für die Tabelle `user_antwort`
--
ALTER TABLE `user_antwort`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `antworten`
--
ALTER TABLE `antworten`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT für Tabelle `fragen`
--
ALTER TABLE `fragen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `user_antwort`
--
ALTER TABLE `user_antwort`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
