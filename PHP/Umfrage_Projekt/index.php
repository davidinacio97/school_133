<?php
/*
 * Binde Controller Klasse ein.
 */
require('controller.php');
/*
 * Binde SiteBuilder Klasse ein.
 */
require('SiteBuilder.php');

/*
 * Starte Sessions
 */
session_start();

/*
 * Deklariere und Initialisiere Controller
 */
$controller = new controller();

/*
 * Deklariere und Initialisiere SiteBuilder
 */
$sitebuilder = new SiteBuilder();

/*
 * Falls ToShow nicht gesetzt, soll die Login-Maske angezeigt werden.
 */
if (!(isset($_GET['ToShow'])))
{
    if($_SESSION['loggedin'] == true)
    {
        header('Location: index.php?ToShow=umfrage#content');
    }
    else
    {
        header('Location: index.php?ToShow=login#content');
    }
}

/*
 * Falls Do gesetzt ist, soll die entsprechende Anforderung ausgeführt werden.
 */
if(isset($_GET['Do'])) {

    switch ($_GET['Do']) {
        case "login":
            $controller->loginUser($_POST['username'], $_POST['password']);
            print_r($_SESSION['userId']);
            break;
        case "register":
            $controller->registUser($_POST['username'], $_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['password']);
            break;
        case "enterSurveyAnswers":
            $controller->enterSurveyAnswers($_POST);
            break;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Eigenes CSS einbinden -->
    <link href="css/mycss.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Oscar Umfrage</title>


</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">Oscar Umfrage</a>
            </div>
                <?php
                    $sitebuilder->get_Menue($_SESSION['loggedin'] == true);
                ?>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-text">
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="content">
        <?php $sitebuilder->get_sitePart($_GET['ToShow']); ?>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; David Dos Reis Inacio
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
</body>

</html>
