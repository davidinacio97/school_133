<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 30.11.2014
 * Time: 15:08
 */

require('errorManager.php');

$errorManager = new errorManager();

// Holt den Wert aus den GET-Daten falls der Key vorhanden ist.
function getValue($getKey)
{
    if(isset($_GET[$getKey]))
    {
        return $_GET[$getKey];
    }
    else
    {
        return "";
    }
}
?>

<script type="text/javascript">

    // Validiert das Formular
    function chkFormular()
    {
        var validUsername =  false;
        var validLastname = false;
        var validFirstname = false;
        var validPassword = false;

        validUsername = checkUsername();
        validLastname = checkLastname();
        validFirstname = checkFirstname();
        validPassword = checkPassword();

        // Falls eine Validierung fehlgeschlagen ist, wird das Formular nicht versendet
        if (validUsername == false || validLastname == false || validFirstname == false || validPassword == false)
        {
            return false;
        }
        return true;

    }

    // Validiert den Usernamen
    function checkUsername()
    {
        // Hole Wert der Textbox
        var usernameText = $('#username').val();

        // Hole Referenz auf ErrorFeld
        var usernameErrorText = $("#errorMessageUsername");

        // Falls Username nicht eingegeben, Fehler ausgeben
        if(usernameText == null || usernameText == "" || usernameText == " ")
        {
            usernameErrorText.html("Geben Sie einen Benutzernamen ein.");
            return false;
        }

        if(usernameText.length != 6)
        {
            usernameErrorText.html("Der Benutzername muss genau 6 Zeichen lang sein.");
            return false;
        }


        // Username darf nur aus alphanummerischen Zeichen bestehen.
        if (!(usernameText.match(/[a-zA-Z0-9]{6}/)))
        {
            usernameErrorText.html("Der Benutzername darf nur aus alphanumerischen Zeichen bestehen.");
            return false;
        }
        return true;
    }

    // Validiert den Nachnamen
    function checkLastname()
    {
        // Hole Wert der Textbox
        var lastnameText = $('#lastname').val();

        // Hole Referenz auf ErrorFeld
        var lastnameErrorText = $("#errorMessageLastName");

        // Falls Nachname nicht eingegeben, Fehler ausgeben
        if(lastnameText == null || lastnameText == "" || lastnameText == " ")
        {
            lastnameErrorText.html("Geben Sie einen Nachnamen ein.");
            return false;
        }

        // Falls Nachname nicht den Anforderungen (nur Buchstaben und mindestens 1 Zeichen) besteht, Fehler ausgeben.
        if (!(lastnameText.match(/[a-zA-Z-\w]{1,}/)))
        {
            lastnameErrorText.html("Der Nachname kann nur aus Buchstaben bestehen.");
            return false;
        }
        return true;
    }

    // Validiert den Vornamen
    function checkFirstname()
    {
        // Hole Wert der Textbox
        var firstnameText = $('#firstname').val();

        // Hole Referenz auf ErrorFeld
        var firstnameErrorText = $("#errorMessageFirstName");

        if (firstnameText == null || firstnameText == "" || firstnameText == " ")
        {
            firstnameErrorText.html("Geben Sie Ihren Vornamen ein.");
            return false;
        }

        // Falls Vorname nicht den Anforderungen (nur Buchstaben und mindestens 1 Zeichen) besteht, Fehler ausgeben.
        if (!(firstnameText.match(/[a-zA-Z- \w]{1,}/)))
        {
            firstnameErrorText.html("Der Vorname kann nur aus Buchstaben bestehen.");
            return false;
        }
        return true;
    }

    // Validiert das Passwort
    function checkPassword()
    {
        // Hole Wert der Textbox
        var passwordText = $('#password').val();

        // Hole Passwort wiederholung
        var passwordRepText = $('#password_rep').val();


        // Hole Referenz auf ErrorFeld
        var passwordErrorRef = $("#errorMessagePassword");

        // Länge des Passwortes prüfen.
        if(passwordText.length < 8)
        {
            passwordErrorRef.html("Passwort muss mindestens 8 Zeichen lang sein.");
            return false;
        }

        // Auf Kleinbuchstaben prüfen.
        if (!(passwordText.match(/[a-z]+/)))
        {
            passwordErrorRef.html("Passwort muss mindestens einen Kleinbuchstaben enthalten.");
            return false;
        }

        //Auf Grossbuchstaben prüfen.
        if (!(passwordText.match(/[A-Z]+/)))
        {
            passwordErrorRef.html("Passwort muss mindestens einen Grossbuchstaben enthalten.");
            return false;
        }

        // Auf Zahlen prüfen.
        if(!(passwordText.match(/[0-9]+/)))
        {
            passwordErrorRef.html("Passwort muss mindestens eine Zahl enthalten.");
            return false;
        }

        // Auf Sonderzeichen prüfen.
        if(/[\x21-\x2F]/.test(passwordText) == false)
        {
            passwordErrorRef.html("Passwort muss mindestens ein Sonderzeichen enthalten.");
            return false;
        }

        // Überprüfen ob beide Passwörter gleich sind.
        if (passwordText != passwordRepText)
        {
            passwordErrorRef.html("Passwörter stimmen nicht überein.");
            return false;
        }

        return true;
    }

</script>

<!-- Contact Section -->
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Registrieren</h2>
                <hr class="star-primary">
            </div>
        </div>
        <div class="row">
            <?php
            if(isset($_GET['error']))
            {
                $errorManager->errorMessage($_GET['error']);
            }

            ?>
            <div class="col-lg-8 col-lg-offset-2">
                <form name="register" id="register" action="index.php?Do=register" method="post" onsubmit="return chkFormular()">

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Benutzername</label>
                            <input type="text" class="form-control" placeholder="Benutzername" id="username" name="username" value="<?php echo getValue("username"); ?>">
                            <p id="errorMessageUsername" class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Name</label>
                            <input type="text" class="form-control" placeholder="Nachname" id="lastname" name="lastname" value="<?php echo getValue("lastname"); ?>">
                            <p id="errorMessageLastName" class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control_group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Vorname</label>
                            <input type="text" class="form-control" placeholder="Vornamen" id="firstname" name="firstname" value="<?php echo getValue("firstname"); ?>">
                            <p id="errorMessageFirstName" class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control_group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>E-Mail</label>
                            <input type="email" class="form-control" placeholder="E-Mail Addresse" id="email" name="email" value="<?php echo getValue("email"); ?>">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control_group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Passwort</label>
                            <input type="password" class="form-control" placeholder="Passwort (erlaubte Sonderzeichen: !, &Prime;, #, $, %, &, ', (, ), *, +, -, ., /)" id="password" name="password">
                            <p id="errorMessagePassword" class="help-block text-danger"></p>
                        </div>
                    </div>

                    <div class="row control_group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Passwort wiederholen</label>
                            <input type="password" class="form-control" placeholder="Passwort wiederholen" id="password_rep">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button type="submit" class="btn btn-success btn-lg">Registrieren</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>