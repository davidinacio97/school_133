<?php
/**
 * Created by PhpStorm.
 * User: vmadmin
 * Date: 02.12.2014
 * Time: 08:09
 */

if($_SESSION['loggedin'] != true)
{
    header('Location: index.php?ToShow=logout');
}

require('errorManager.php');
$errorManager = new errorManager();


$db = new database();

    /* Falls Benutzer Umfrage bereits ausgefüllt hat, auf Auswertung weiterleiten. */
    if($db->userFilledSurvey($_SESSION['userId']))
    {
        header('Location: index.php?ToShow=auswertung&error=surveyAlreadyFilled');
    }

?>

<script type="text/javascript">

    function chkUmfrage()
    {
        return true;
    }
</script>

<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Umfrage</h2>
                <hr class="star-primary">
            </div>
        </div>
        <div class="row">
            <?php
                if(isset($_GET['error']))
                {
                    $errorManager->errorMessage($_GET['error']);
                }

            ?>

            <div class="col-lg-8 col-lg-offset-2">
                <form name="umfrage" id="umfrage" action="index.php?Do=enterSurveyAnswers" method="post" onsubmit="return chkUmfrage()">

                    <?php
                    // Hole alle Fragen
                    $result = $db->getQustions();

                    // Falls Fragen vorhanden.
                    if ($result != false)
                    {
                        // Speichere die Fragen in die Session-Variable 'questions'
                        $_SESSION['questions'] = $result;

                        // Für jede Frage sollen die Möglichen Antworten geholt und angezeigt werden.
                        foreach ($result as $x)
                        {
                            echo '<div class="form-group col-xs-12"><label class="controls">'. $x['frage'].'</label>';

                            // Hole mögliche Antworten für Frage.
                            $answers = $db->getAnswers($x['id']);
                            // Falls Antworten verfügbar sind.
                            if ($answers != false)
                            {
                                // Jede Antwort anzeigen.
                                foreach ($answers as $a)
                                {
                                    // Als Value wird die Antwort-Id gespeichert.
                                    echo '<div class="control-group"><label class="radio-inline"><input type="radio" class="" value="'.$a['id'].'" required name="'.$x['name'].'" /> '.$a['antwort'].'</label></div>';
                                    echo '<p class="help-block text-danger"></p>';
                                }
                                echo '</div>';
                            }
                        }
                    }
                    ?>
                    <br>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button type="submit" name="saveUmfrage" class="btn btn-success btn-lg">Speichern</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</section>


