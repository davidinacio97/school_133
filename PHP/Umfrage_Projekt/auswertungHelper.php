<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 21.12.2014
 * Time: 18:21
 */

/**
 * Class auswertungHelper Diese Klasse hilft bei der Auswertung, es wird die Frage-Id, Antwort-Id und der Anteil gespeichert.
 */
class auswertungHelper {

    /**
     * @var Speichert die Frage-Id
     */
    public $questionId;

    /**
     * @var Speichert die Antwort-Id
     */
    public $answerId;

    /**
     * @var Speichert den Anteil
     */
    public $percantage;
} 