<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 14.12.2014
 * Time: 19:01
 */

if($_SESSION['loggedin'] != true)
{
    header('Location: index.php?ToShow=logout');
}

/**
 * Instanziiert Controller
 */
$controller = new controller();


/* Falls Benutzer Umfrage nicht ausgefüllt hat, auf Umfrage weiterleiten. */
if(!($controller->userFilledSurvey($_SESSION['userId'])))
{
    header('Location: index.php?ToShow=umfrage&error=fillSurveyFirst');
}


/**
 * Importiere ErrorManager.
 */
require('errorManager.php');

/**
 * Instanziiere ErrorManager
 */
$errorManager = new errorManager();

?>


<section id="auswertung">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Auswertung</h2>
                <hr class="star-primary">
            </div>
        </div>
        <?php
            /* Falls ein Fehlercode übergeben wurde, diesen Ausgeben. */
            if(isset($_GET['error']))
            {
                $errorManager->errorMessage($_GET['error']);
            }
        ?>
        <div class="row">
            <?php
                /*
                 *  Erstellt die Auswertung in Tabellenform.
                */
                $questions = $controller->getQuestions();

                /*
                 * Für jede Frage werden die Daten via Controller geholt.
                 */
                foreach($questions as $q)
                {
                    echo "<table class='table table-hover' border='1' style='border-color: #161f29' >";
                    echo "<tbody>";
                    echo "<div class='row'>";

                    $questionsText = $controller->getAuswertungText($q['id']);
                    if($questionsText != false)
                    {
                        echo "<div class='row'>";
                        echo "<tr>";
                        echo "<td width='100%' style='text-align: center; background: #2C3E50; color: #ffffff'  >";
                        echo $questionsText;
                        echo "</td>";
                        echo "</tr>";
                    }

                    echo "</div>";

                    echo "</tbody>";
                    echo "</table>";

                    echo "<table class='table table-hover'>";
                    echo "<tbody>";
                    echo "<div class='row'>";

                    $controller->getResult($q['id']);

                    echo "</div>";

                    echo "</tbody>";
                    echo "</table>";

                }
            ?>
        </div>
    </div>
</section>