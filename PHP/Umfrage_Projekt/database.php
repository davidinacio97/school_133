<?php
/**
 * Created by PhpStorm.
 * User: David Dos Reis Inacio
 * Date: 30.11.2014
 * Time: 15:25
 */

/**
 * Class database Datenbank-Klasse, stellt die Verbindung zur Datenbank her.
 */
class database
{
    /**
     * @var Enthält die Anmeldeinformationen zur Datenbankverbindung
     */
    private $ini_array;

    /**
     * @var PDO-Objekt
     */
    private $dbh;

    /**
     * Konstruktor, initialisiert die Datenbankverbindung
     */
    function  __construct()
    {
        $ini_array = parse_ini_file("database.ini");
        $this->dbh = new PDO('mysql:host=localhost;dbname=umfrageprojekt;charset=utf8', $ini_array['username'], $ini_array['password']);
    }

    /**
     *  Überprüft ob ein Benutzername vorhanden ist.
     * @param $username Benutzername der überprüft werden soll.
     * @return bool Boolean, ob der Benutzername vorhanden ist.
     */
    public function existsUsername($username)
    {
        try
        {
            $abfrage = $this->dbh->query("SELECT COUNT(username) FROM `user` WHERE username = '". $username."'");

            if ($abfrage == false)
            {
                return true;
            }

            return $abfrage->fetchColumn() > 0;
        }
        catch (PDOException $e)
        {
            return true;
        }
    }

    /**
     * Login-Funktion, überpüft ob Benutzername und Passwort einem vorhandenem User zugeordnet werden können.
     * @param $user Benutzername
     * @param $userPassword Passwort
     * @return bool Boolean, ob der User in dieser Kombination exisitiert.
     */
    public function existsUser($user, $userPassword)
    {
        try
        {
            $abfrage = $this->dbh->query("SELECT `password` FROM `user` WHERE username = '".$user."'");

            if ($abfrage == false)
            {
                return false;
            }

            $password = $abfrage->fetch(PDO::FETCH_ASSOC)['password'];

            if ($password  == md5($userPassword))
            {
                return true;
            }
        }
        catch (PDOException $e)
        {
            return false;
        }

    }

    /**
     * Registriert einen neuen User in der Datenbank.
     * @param $username Benutzername
     * @param $firstname Vorname
     * @param $lastname Nachname
     * @param $email E-Mail
     * @param $password Passwort
     * @return bool Boolean, ob die Registrierung erfolgreich war.
     */
    public function registUser($username, $firstname, $lastname, $email, $password)
    {
        try
        {
            $sql_query = "INSERT INTO `user` (username, firstname, lastname, email, password) VALUES ('".$username."', '".$firstname."', '".$lastname."', '".$email."', '".md5($password)."')";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false)
            {
                return false;
            }

            return true;
        }
        catch (PDOException $e)
        {
            return false;
        }

    }

    /**
     * Gibt alle Fragen der Umfrage zurück, falls die Abfrage nicht erfolgreich war, False zurück geben.
     * @return array|bool Array falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function getQustions()
    {
        try
        {
            $sql_query = "SELECT * FROM fragen";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false)
            {
                return false;
            }

            return $abfrage->fetchAll();

        }
        catch (PDOException $e)
        {

        }
    }

    /**
     * Gibt die Möglichen Antworten auf eine Frage zurück.
     * @param $questionID Id der Frage
     * @return array|bool Array falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function getAnswers($questionID)
    {
        try
        {
            $sql_query = "SELECT * FROM antworten WHERE fk_frage = '".$questionID."'";
            $abfrage = $this->dbh->query($sql_query);

            if ($abfrage == false)
            {
                return false;
            }

            return $abfrage->fetchAll();

        }
        catch(PDOException $e)
        {
            return false;
        }
    }

    /**
     * Gibt den Text zur Antwort-Id zurück.
     * @param $answerId Antwort-Id
     * @return bool Text falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function getAnswerText($answerId)
    {
        try
        {
            $sql_query = "SELECT antwort FROM antworten WHERE id= '".$answerId."'";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false)
            {
                return false;
            }

            return $abfrage->fetch(PDO::FETCH_ASSOC)['antwort'];
        }
        catch(PDOException $e)
        {

        }
    }

    /**
     * Gibt die Id der Frage auf eine Antwort-Id zurück.
     * @param $answer Antwort-Id
     * @return bool Frage-Id falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function getQuestionFromAnswer($answer)
    {
        try
        {
            $sql_query = "SELECT fk_frage FROM antworten WHERE antwort='".$answer."'";
            $abfrage = $this->dbh->query($sql_query);

            if ($abfrage == false)
            {
                return false;
            }

            return $abfrage->fetch(PDO::FETCH_ASSOC)['fk_frage'];
        }
        catch (PDOException $e)
        {
            return false;
        }
    }

    /**
     * Speichert die Resultate der Frage eines Benutzers
     * @param $userId Benutzer-Id
     * @param $frageId Frage-Id
     * @param $antwortId Antwort-Id
     * @return bool Bool ob Abfrage erfolgreich.
     */
    public function enterAnswer($userId, $frageId, $antwortId)
    {
        try
        {
            $sql_query = "INSERT INTO user_antwort (fk_userId, fk_frageId, fk_antwortId) VALUES ('".$userId."', '".$frageId."', '".$antwortId."')";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false)
            {
                return false;
            }

            return true;

        }
        catch (PDOException $e)
        {

        }
    }

    /**
     * Gibt die User-Id eines Benutzernamen zurück.
     * @param $username Benutzername
     * @return bool User-Id falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function getUserId($username)
    {
        try
        {
            $sql_query = "SELECT id  FROM `user`  WHERE username='".$username."'";
            $abfrage = $this->dbh->query($sql_query);

            if ($abfrage == false)
            {
                return false;
            }

            return $abfrage->fetch(PDO::FETCH_ASSOC)['id'];
        }
        catch (PDOException $e)
        {
            return false;
        }
    }

    /**
     * Gibt zurück ob der Benutzer die Umfrage bereits ausgefüllt hat.
     * @param $userid Benutzer-Id
     * @return bool Boolean ob der Benutzer die Umfrage ausgefüllt hat.
     */
    public function userFilledSurvey($userid)
    {
        try
        {
            $sql_query = "SELECT COUNT(fk_userId) FROM user_antwort WHERE fk_userId='".sha1($userid)."'";
            $abfrage = $this->dbh->query($sql_query);
            return $abfrage->fetchColumn() > 0;

        }
        catch (PDOException $e)
        {
            return true;
        }
    }

    /**
     * Gibt die Anzahl Kombinationen aus Frage-Id und Antwort-Id zurück.
     * @param $questionId Frage-Id
     * @param $answerId Antwort-Id
     * @return bool|string Anzahl falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function countQuestionAnswers($questionId, $answerId)
    {
        try
        {
            $sql_query = "SELECT COUNT(*) FROM user_antwort WHERE fk_frageId = '".$questionId."' AND fk_antwortId = '".$answerId."'";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false)
            {
                return false;
            }

            return $abfrage->fetchColumn();
        }
        catch(PDOException $e)
        {

        }
    }

    /**
     * Gibt die Anzahl Antworten auf eine Frage zurück.
     * @param $questionId Frage-Id
     * @return bool|string Anzahl falls Abfrage erfolgreich, andernfalls False zurückgeben.
     */
    public function countSameQuestionAnswered($questionId)
    {
        try
        {
            $sql_query = "SELECT COUNT(*) FROM user_antwort WHERE fk_frageId = '".$questionId."'";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false)
            {
                return false;
            }
            return $abfrage->fetchColumn();
        }
        catch(PDOException $e)
        {

        }
    }

    /**
     * Gibt den Text für die Auswertung zurück, welcher der Frage entspricht.
     * @param $questionId Frage-Id
     * @return bool
     */
    public function getQuestionText($questionId)
    {
        try
        {
            $sql_query = "SELECT text FROM auswertung_text WHERE fk_frageId = '".$questionId."'";
            $abfrage = $this->dbh->query($sql_query);

            if($abfrage == false) {
                return false;
            }
            return $abfrage->fetch(PDO::FETCH_ASSOC)['text'];
        }
        catch(PDOException $e)
        {
            return false;
        }
    }

}



