<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 30.11.2014
 * Time: 17:54
 */

/**
 * Class SiteBuilder Gibt den gewünschten Content zurück.
 */
class SiteBuilder
{
    /**
     * Gibt den gewünschten Content zurück.
     * @param $part Content-Beschreibung
     * @return mixed Content als PHP Datei.
     */
    public function get_sitePart($part)
    {
        switch ($part)
        {
            case "login":
                return include("login.php");
            case "register":
                return include("register.php");
            case "umfrage":
                return include("umfrage.php");
            case "logout":
                return include("logout.php");
            case "auswertung":
                return include("auswertung.php");
        }
    }

    /**
     * Gibt das korrekte Menü zurück, abhängig ob de User eingeloggt ist.
     * @param $isLoggedIn Boolean, ob der User eingeloggt ist.
     * @return mixed Menü als PHP Datei
     */
    public function get_Menue($isLoggedIn)
    {
        if($isLoggedIn == true)
        {
            return include("menue_loggedIn.php");
        }
        else
        {
            return include("menue_notloggedIn.php");
        }
    }
} 