<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 30.11.2014
 * Time: 17:29
 */
require('errorManager.php');
$errorManager = new errorManager();

?>

<script type="text/javascript">

    // Überprüft rudimentär auf Fehler in den Anmeldedaten
    function chkFormular()
    {
        // Hole Wert der Textbox
        var usernameText = $('#username').val();
        // Hole Referenz auf Fehlerfeld
        var usernameErrorTextRef = $('#errorMessageUsername');

        // Falls Username nicht eingegeben, Fehler ausgeben
        if(usernameText == null || usernameText == "" || usernameText == " ")
        {
            usernameErrorTextRef.html("Geben Sie Ihren Benutzernamen ein.");
            return false;
        }

        if(usernameText.length != 6)
        {
            usernameErrorTextRef.html("Geben Sie einen gültigen Benutzernamen ein.");
            return false;
        }

        var passwordText = $('#password').val();
        var passwordErrorRef = $('#errorMessagePassword');
        if(passwordText == null || passwordText == "" || passwordText == " ")
        {
            passwordErrorRef.html("Geben Sie Ihr Passwort ein.");
            return false;
        }
        return true;
    }


</script>

<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Login</h2>
                <hr class="star-primary">
            </div>
        </div>
        <?php
            if(isset($_GET['error'])) {
                $errorManager->errorMessage($_GET['error']);
            }
        ?>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">


                <form name="login" id="login" action="index.php?Do=login" method="post" onsubmit="return chkFormular()">
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Benutzername</label>
                                <input type="text" class="form-control" placeholder="Name" id="username" name="username">
                                <p id="errorMessageUsername" class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Passwort</label>
                                <input type="password" class="form-control" placeholder="Passwort" id="password" name="password">
                                <p id="errorMessagePassword" class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button type="submit" class="btn btn-success btn-lg">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>